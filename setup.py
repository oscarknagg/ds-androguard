#!/usr/bin/env python

from distutils.core import setup

setup(name='dsandroguard',
    version='0.0',
    description='Data Science installable androguard package',
    author='Oscar Knagg',
    author_email='oscar.knagg@wandera.com',
    url='',
    packages=['dsandroguard'],
)