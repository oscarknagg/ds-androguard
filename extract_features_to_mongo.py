from dsandroguard.androlyse import extract_features_from_apk
from pymongo import MongoClient
import argparse
import hashlib
import pprint
import os
import config


def calculate_hashes(infile):
    hashes = []
    for hash_function in ['md5','sha1','sha256']:
        hashes.append(calculate_hash(infile, hash_function))
    return tuple(hashes)


def calculate_hash(infile,hash_function):
    blocksize = 65536
    if hash_function == 'md5':
        hasher = hashlib.md5()
    elif hash_function == 'sha1':
        hasher = hashlib.sha1()
    elif hash_function == 'sha256':
        hasher = hashlib.sha256()

    try:
        infile.seek(0)
    except:
        pass

    buf = infile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = infile.read(blocksize)
    return hasher.hexdigest()


# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('target')
parser.add_argument('--collection',default='apk_temp')
args = parser.parse_args()

# Connect to mongo
MONGO_URI = config.mongo_uri
client_ds = MongoClient(MONGO_URI,connect=False)
db = client_ds.apk

if os.path.isdir(os.path.abspath(args.target)):
    print('Attempting to extract androguard features for all files in folder:')
    print(os.path.abspath(args.target))
    print('')
    for root, _, files in os.walk(os.path.abspath(args.target)):
        for f in files:
            try:
                with open(os.path.join(root, f)) as openfile:
                    apk_entry = extract_features_from_apk(openfile, True)
                    hashes = calculate_hashes(openfile)
                apk_entry.update({'md5': hashes[0]})
                apk_entry.update({'sha1': hashes[1]})
                apk_entry.update({'sha256': hashes[2]})
                pprint.pprint(apk_entry)
                print('')
                db[args.collection].insert_one(apk_entry)
            except Exception, e:
                print('EXCEPTION: ')
                print(str(e))
else:
    print('Attempting to extract androguard features for:')
    print(os.path.abspath(args.target))
    print('')
    try:
        with open(os.path.abspath(args.target)) as openfile:
            apk_entry = extract_features_from_apk(openfile, True)
            hashes = calculate_hashes(openfile)
        apk_entry.update({
            'md5': hashes[0],
            'sha1': hashes[1],
            'sha256': hashes[2],
            '_id': hashes[2]
        })
        pprint.pprint(apk_entry)
        print('')
        db[args.collection].insert_one(apk_entry)
    except Exception, e:
        print('EXCEPTION: ')
        print(str(e))
