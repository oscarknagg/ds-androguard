#!/usr/bin/env python

# This file is part of Androguard.
#
# Copyright (C) 2012/2013/2014, Anthony Desnos <desnos at t0t0.fr>
# All rights reserved.
#
# Androguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Androguard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Androguard.  If not, see <http://www.gnu.org/licenses/>.

from dsandroguard.androguard.core.bytecodes.apk import APK
from dsandroguard.androguard.misc import AnalyzeDex


def extract_features_from_manifest(manifest_string, feature_type):
    features = []
    type_dict = {
        'receiver'   : 'receiver',
        'activity'   : 'activity',
        'intent'     : 'action',
        'service'    : 'service',
        'permission' : 'uses-permission'
    }
    substring = '<' + type_dict[feature_type]
    for i in find_substring_indexes(manifest_string,substring):
        j = 0
        scanstring = 'android:name="'
        while True:
            j += 1
            if manifest_string[i+len(substring)+j:i+len(substring)+j+len(scanstring)] == scanstring:
                k = 0
                feature = ''
                while True:
                    feature += manifest_string[i+len(substring)+j+len(scanstring)+k]
                    k += 1
                    if manifest_string[i+len(substring)+j+len(scanstring)+k] == '"':
                        features.append(feature)
                        break
                break
    return list(set(features))


def find_substring_indexes(string, substring):
    start = 0
    while True:
        start = string.find(substring, start)
        if start == -1: return
        yield start
        start += len(substring)


def get_urls(list_of_strings):
    urls = []
    for i in list_of_strings:
        if 'http:' in i or 'https:' in i or 'www' in i:
            urls.append(i)
    return list(set(urls))


def extract_features_from_apk(apk_file, manifest_only=True):
    a = APK(apk_file.read(), raw=True)
    print 'Got APK'

    manifest = a.get_AndroidManifest()
    manifest_string = manifest.toprettyxml()
    intents = extract_features_from_manifest(manifest_string, 'intent')

    if manifest_only:
        features = {
            "package_name": a.get_package(),
            "app": a.get_app_name(),
            "displayed_version": a.get_androidversion_name(),
            "min_sdk_version":a.get_min_sdk_version(),
            "target_sdk_version": a.get_target_sdk_version(),
            "max_sdk_version": a.get_max_sdk_version(),
            "androguard": {
                "activities": a.get_activities(),
                "filters": intents,
                "libraries": a.get_libraries(),
                "new_permissions": a.get_declared_permissions(),
                "permissions": a.get_requested_permissions(),
                "providers": a.get_providers(),
                "receivers": a.get_receivers(),
                "services": a.get_services()
            }
        }
        return features
    else:
        print 'Decompiling'
        d, dx = AnalyzeDex(a.get_dex(), raw=True, decompiler="dad")
        print 'Decompiled.'
        methods = list(set([(i.get_class_name()[:-1] + '.' + i.get_name() + '()') for i in d.get_methods()]))
        classes = list(set([i.get_class_name()[:-1] for i in d.get_methods()]))
        features = {
            "package_name": a.get_package(),
            "app": a.get_app_name(),
            "displayed_version": a.get_androidversion_name(),
            "min_sdk_version":a.get_min_sdk_version(),
            "target_sdk_version": a.get_target_sdk_version(),
            "max_sdk_version": a.get_max_sdk_version(),
            "androguard": {
                "activities": a.get_activities(),
                "filters": intents,
                "libraries": a.get_libraries(),
                "new_permissions": a.get_declared_permissions(),
                "permissions": a.get_requested_permissions(),
                "providers": a.get_providers(),
                "receivers": a.get_receivers(),
                "services": a.get_services(),
                "methods": methods,
                "classes": classes
            }
        }
        return features
